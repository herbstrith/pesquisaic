#include "PajeCudaSimulator.h"
#include "PajeException.h"
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>


PajeCudaSimulator::PajeCudaSimulator ()
{
  stopSimulationAtTime = -1;
  init ();
}

void PajeCudaSimulator::init (void)
{
  
  rootType = new PajeContainerType ("0", "0", NULL);
  root = new PajeContainer (0, "0", "0", NULL, rootType, NULL);
  typeMap[rootType->identifier()] = rootType;
  typeNamesMap[rootType->name()] = rootType;
  contMap[root->identifier()] = root;
  contNamesMap[root->name()] = root;
  lastKnownTime = -1;

  selectionStart = -1;
  selectionEnd = -1;
  
  eventsPoolSize = 500;
  events.reserve(eventsPoolSize);
  eventsPoolCounter = 0;
}


PajeCudaSimulator::~PajeCudaSimulator() {
  delete root;
  delete rootType;
  typeMap.clear ();
  typeNamesMap.clear ();
  contMap.clear();
  contNamesMap.clear();
}

void PajeCudaSimulator::inputEntity(PajeObject *data) {
  //get event, set last known time
  PajeTraceEvent *event = (PajeTraceEvent*)data;
  
  events.push_back(event);
  eventsPoolCounter++;
  
  if(eventsPoolCounter >= eventsPoolSize) {
    // copy to device memory and call kernel
    
    eventsPoolCounter = 0;
  }
}


void PajeCudaSimulator::loadDevice(void) {
  // load events into the gpu
}

void PajeCudaSimulator::unloadDevice(void) {
  // move events from the gpu to the host
}


void PajeCudaSimulator::startReading(void) {
  
}

void PajeCudaSimulator::finishedReading(void) {
}
