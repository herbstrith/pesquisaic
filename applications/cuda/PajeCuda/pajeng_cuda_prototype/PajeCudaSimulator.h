/*
    This file is part of PajeCuda

    PajeNG is free software: you can redistribute it and/or modify
    it under the terms of the GNU Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PajeNG is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Public License for more details.

    You should have received a copy of the GNU Public License
    along with PajeNG. If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __PAJECUDASIMULATOR_H__
#define __PAJECUDASIMULATOR_H__
#include <map>
#include <vector>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "PajeTraceEvent.h"
#include "PajeEvent.h"
#include "PajeType.h"
#include "PajeContainer.h"
#include "PajeComponent.h"

#define CALL_MEMBER_PAJE_SIMULATOR(object,ptr) ((object).*(ptr))

class PajeCudaSimulator : public PajeComponent {
private:
  PajeContainerType *rootType;
  PajeContainer *root;
  std::map<std::string,PajeType*> typeMap;
  std::map<std::string,PajeType*> typeNamesMap; //for names
  std::map<std::string,PajeContainer*> contMap;
  std::map<std::string,PajeContainer*> contNamesMap; //for names
  

  double stopSimulationAtTime;

  void init (void);
  
  void loadDevice(void);
  void unloadDevice(void);
  std::vector<PajeTraceEvent*> events; 
  string[] events_strings;
public:
  int eventsPoolSize;

private:

  double selectionStart;
  double selectionEnd;
  
  int eventsPoolCounter;

protected:
  double lastKnownTime;


public:
  PajeCudaSimulator();
  ~PajeCudaSimulator();
  void report (void);
  void reportDotFormat (void);
  void reportContainer (void);
  bool keepSimulating (void);
  
  void inputEntity (PajeObject *data);
  void startReading (void);
  void finishedReading (void);
  
  void startKernel();

};
#endif
