#include "PajeComponent.h"
#include "PajeEventDecoder.h"
#include "PajeSimulator.h"
#include "PajeEventDecoder.h"
#include <sys/time.h>
#include "PajeUnity.h"
#include "PajeException.h"
#include "PajeFileReader.h"
#include "PajeProbabilisticSimulator.h"




PajeComponent *reader;
PajeEventDecoder *decoder;
PajeSimulator *simulator;
PajeDefinitions *definitions;
bool flexReader;
double t1, t2;


static double gettime(void) {
  struct timeval tr;
  gettimeofday(&tr, NULL);
  return (double)tr.tv_sec+(double)tr.tv_usec/1000000;
}


int main(void) {

  //basic configuration
  flexReader = flexReader;
  bool strictHeader = 1;
  std::string tracefilename = "teste.paje";
  int stopat = 0;
  int ignoreIncompleteLinks = 0;
  char *probabilistic = NULL;
  //the global PajeDefinitions object
  definitions = new PajeDefinitions (strictHeader);
 
  try {
    //alloc reader
    if (flexReader){
      if (tracefilename.empty()){
	//reader = new PajeFlexReader(definitions);
      }else{
	//reader = new PajeFlexReader(tracefilename, definitions);
      }
    }else{
      if (tracefilename.empty()){
	reader = new PajeFileReader();
      }else{
        reader = new PajeFileReader (tracefilename);
      }
    }

    //alloc decoder and simulator
    if (!flexReader){
      decoder = new PajeEventDecoder(definitions);
    }
    if (probabilistic){
      simulator = new PajeProbabilisticSimulator (probabilistic);
    }else{
      simulator = new PajeSimulator (stopat, ignoreIncompleteLinks);
    }


    //connect components
    if (flexReader){
      reader->setOutputComponent (simulator);
      simulator->setInputComponent (reader);
    }else{
      reader->setOutputComponent (decoder);
      decoder->setInputComponent (reader);
      decoder->setOutputComponent (simulator);
      simulator->setInputComponent (decoder);
    }
    //simulator->setOutputComponent (this);
    //this->setInputComponent (simulator);
  }catch (PajeException& e){
    e.reportAndExit ();
  }

  //read and simulate
  t1 = gettime();
  try {
    reader->startReading ();
    while (reader->hasMoreData() && simulator->keepSimulating()){
      reader->readNextChunk ();
    }
    reader->finishedReading ();
  }catch (PajeException& e){
    e.reportAndExit();
  }
  t2 = gettime();
}
