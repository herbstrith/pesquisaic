#include <stdio.h>
#include <cuda.h>

const int N = 16; 
const int blocksize = 16; 

//used to compare strings
__device__ __host__
int cmp4(const char4 & c1, const char4 & c2)
{
    int result;

    result = c1.x - c2.x; if (result !=0) return result; 
    result = c1.y - c2.y; if (result !=0) return result; 
    result = c1.z - c2.z; if (result !=0) return result; 
    result = c1.w - c2.w; if (result !=0) return result; 

    return 0;
}

//used to compare strings
__device__ __host__
int strncmp4(const char4 * s1, const char4 * s2, const unsigned int nwords)
{
    for(unsigned int i=0; i<nwords; i++) {
        int result = cmp4(s1[i], s2[i]);
        if (result != 0) return result;
    }

    return 0;
}





// Kernel that executes on the CUDA device
__global__ void pajeDefineContainerType(float *a, int N)
{
  int idx = threadIdx.x + blockIdx.x * blockDim.x;

    char4 * s1 = a.data + a.pos[idx];
    char4 * s2 = b.data + b.pos[idx];
    unsigned int slen = min(a.length[idx], b.length[idx]);

    result[idx] = strncmp4(s1, s2, slen);
}


// main routine that executes on the host
int main(void)
{
  char *string_a;
  char *string_b;

  checkCudaErrors(cudaMalloc( (void**)&string_a, M * sizeof(char) ));
  checkCudaErrors(cudaMalloc( (void**)&string_b, M * sizeof(char) ));
}