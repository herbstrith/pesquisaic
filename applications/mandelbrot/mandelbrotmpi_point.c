#include <stdio.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <time.h>

const int iXmax = 800; 
const int iYmax = 800;

int main(argc,argv)
{
	int calculatedPoint[iYmax][iXmax][3];//the image to print

	//Algorithm variables akin to the mandelbrot algorithm found on wikipedia
	
	/* screen ( integer) coordinate */
	int iX,iY;

	/* world ( double) coordinate = parameter plane*/
	double Cx,Cy;
	const double CxMin=-2.5;
	const double CxMax=1.5;
	const double CyMin=-2.0;
	const double CyMax=2.0;
	/* */
	double PixelWidth=(CxMax-CxMin)/iXmax;
	double PixelHeight=(CyMax-CyMin)/iYmax;
	/* color component ( R or G or B) is coded from 0 to 255 */
	/* it is 24 bit color RGB file */
	const int MaxColorComponentValue=255; 
	FILE * fp;
	char *filename="new1.ppm";
	char *comment="# ";/* comment should start with # */
	static unsigned char color[3];
	/* Z=Zx+Zy*i  ;   Z0 = 0 */
	double Zx, Zy;
	double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
	/*  */
	int Iteration;
	const int IterationMax=200;
	/* bail-out value , radius of circle ;  */
	const double EscapeRadius=2;
	double ER2=EscapeRadius*EscapeRadius;


	//My MPI variables start
	int rank;
	int processMatrixStart;
	int processMatrixEnd;
	int numberProcesses;
	//////////////////////////////////////////////////////

	MPI_Init(&argc, &argv );
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &numberProcesses);
	processMatrixStart = rank * iYmax/numberProcesses;
  processMatrixEnd = (rank+1) * iYmax/numberProcesses;
  printf("\n process %d doing %d %d",rank, processMatrixStart,processMatrixEnd);
	
  //calculate colors akin to mandelbrot algorithm found on wikipedia
  //but it is distributed between the processes
  for(iY = processMatrixStart;iY < processMatrixEnd;iY++)
  {	
    Cy=CyMin + iY*PixelHeight;
    if (fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
    for(iX=0;iX<iXmax;iX++)
    {         
      Cx=CxMin + iX*PixelWidth;
      /* initial value of orbit = critical point Z= 0 */
      Zx=0.0;
      Zy=0.0;
      Zx2=Zx*Zx;
      Zy2=Zy*Zy;
      for(Iteration=0;Iteration<IterationMax && ((Zx2+Zy2)<ER2);Iteration++)
      {
        Zy=2*Zx*Zy + Cy;
        Zx=Zx2-Zy2 +Cx;
        Zx2=Zx*Zx;
        Zy2=Zy*Zy;
      }
      /* compute  pixel color (24 bit = 3 bytes) */
      if (Iteration==IterationMax)
      { /*  interior of Mandelbrot set = black */
         calculatedPoint[iY][iX][0]=0;
         calculatedPoint[iY][iX][1]=0;
         calculatedPoint[iY][iX][2]=0;                           
      }
      else 
      { /* exterior of Mandelbrot set = white */
         calculatedPoint[iY][iX][0]=255; /* Red*/
         calculatedPoint[iY][iX][1]=255;  /* Green */ 
         calculatedPoint[iY][iX][2]=255;/* Blue */
      }
    }
    //  printf("done the y %d \n",iY);           
  }
       
  if(rank != 0)
  {
  	printf("\n %d has ended its part", rank);
  	MPI_Send(calculatedPoint[processMatrixStart],(processMatrixEnd-processMatrixStart)*iXmax*3,MPI_INT,0,1, MPI_COMM_WORLD );
  }
  
  //master prints the image
  if(rank==0)
	{
		if(numberProcesses > 1)
		{
			int process = 0;
			//recv the messages from the other process in order
			//****theres probably a better way ****//
			for(process =1; process < numberProcesses;process++)
			{
				MPI_Recv(calculatedPoint[process*(iYmax/numberProcesses)],(iYmax/numberProcesses)*iXmax*3,MPI_INT,process,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			}
		}
		printf("\nPrinting to file...");
    fp= fopen(filename,"wb"); /* b -  binary mode */
    /*write ASCII header to the file*/
    fprintf(fp,"P6\n %s\n %d\n %d\n %d\n",comment,iXmax,iYmax,MaxColorComponentValue);
    int i =0;
    int j =0;
    for(i =0; i < iYmax;i++)
    {
		  for(j =0; j < iXmax;j++)
		  {
		   	fwrite(calculatedPoint[i][j],1,3,fp);
		  }
    }
   fclose(fp);
   printf("Finish");
  }

  MPI_Finalize();
  return 0;
    
    
 }