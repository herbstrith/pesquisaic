\documentclass{ieee}
\usepackage{blindtext, graphicx}
\usepackage{times}
\usepackage{graphicx,url}
\usepackage{floatsty}
\usepackage[brazil]{babel}   
\usepackage[utf8]{inputenc}

\usepackage{color}
\newcommand{\review}[1]{\textcolor[rgb]{1,0,0}{[#1]}}
\newcommand{\answer}[1]{\textcolor[rgb]{0,1,0}{[#1]}}
\newcommand{\Paje}{Pajé\xspace}
\newcommand{\PajeNG}{\texttt{PajeNG}\xspace}

\begin{document}

\title{Uma Proposta de um Formato Binário de Rastro Pajé}


\author{Vinícius A.Herbstrith, Lucas Mello Schnorr\\ \{vaherbstrith,
schnorr\}@inf.ufrgs.br\\ Instituto de Informática – Universidade
Federal do Rio Grande do Sul (UFRGS)\\ Caixa Postal 15.064 –
91.501-970 – Porto Alegre – RS – Brazil\\ }

\maketitle \thispagestyle{empty}

\begin{abstract}
 Este artigo apresenta um formato binário para arquivos de rastro em \Paje, almejando
 principalmente a otimização da leitura através da
 estrutura binária, de formar a proporcionar um ganho de desempenho para ferramentas
 que fazem uso do formato para análise e visualização de rastros. A proposta inclui a
 implementação de dois conversores entre o formato textual e binário e um componente que promove 
 a leitura do arquivo de rastro binário integrado na ferramenta de 
 visualização \PajeNG e os ganhos em desempenho da leitura do formato binário
 em relação ao formato textual.
\end{abstract}



\section{Introdução}

Com a difusão de aplicações paralelas e distribuídas surge a
necessidade da criação de ferramentas que auxiliam na depuração e
análises de desempenho destas aplicações. Para tal, se faz a seleção
de eventos representativos que devem ser registrados durante a
execução em arquivos de rastro de
execução~\cite{stein2001depuracao}. Dentre as informações que compõem
estes eventos, temos identificação, tempo do registro, estados de
variáveis e outros dados que sejam de interesse para a análise da
aplicação em questão.

O formato de rastro Pajé é largamente utilizado para a análise e
visualização de rastros de aplicações. O atributo de destaque do
formato é a extensibilidade, possibilitando seu uso nos mais variados
ambientes de execução, com demandas e requisitos diferenciados, sem
ser necessário modificar componentes internos da ferramenta Pajé. Como
ponto negativo do formato, temos a sua representação textual, que
implica em impactos negativos no desempenho tanto no momento de
registro de eventos, gerando uma maior intrusão, como durante a
leitura dos arquivos de rastro enquanto processados para a sua
visualização e análise. Com o crescente aumento do poder de
paralelismo dos computadores de hoje em dia~\cite{Exascale2011},
combinado com aplicações mais complexas que geram um número muito
grande de eventos durante sua execução, os pontos negativos do Pajé
acabam se sobressaindo ainda mais, apresentando uma fraca
escalabilidade.

Este artigo apresenta um formato binário para o arquivo de rastro
Pajé, como forma de obter uma otimização na leitura de arquivos Pajé. A
seção~\ref{Desenvolvimento} trata da geração dos arquivos em binário,
fazendo uso da biblioteca de rastros libRastro~\cite{Jrastro} e
Poti~\cite{Poti}, e a implementação da leitura destes arquivos na
ferramenta de visualização de rastros PajeNG ~\cite{PajeNG}. A
seção~\ref{sec:codificacao} apresentam com mais detalhes a
representação dos eventos Pajé em seu novo formato binário. Por fim, a
seção~\ref{Resultados} apresenta os resultados dos experimentos feitos
sobre a leitura de arquivos de rastros e a comparação dos tempos do
formato binário contra a representação Pajé textual padrão.


\section{Proposta e implementação} \label{Desenvolvimento}


A proposta inclui a implementação de dois conversores e um componente
de leitura binário para a ferramenta de visualização PajeNG. Estes
três componentes são detalhados na Figura~\ref{fig:Poti_components} em pontilhado,
onde temos os dois conversores ({\it paje2rst} e {\it rst2paje}),
desenvolvidos dentro da biblioteca Poti com a linguagem C, e o leitor
binário ({\it PajeRastroReader}), desenvolvido na ferramenta PajeNG em
C++. Os outros componentes são aqueles que já fazem parte da
PajeNG.


\begin{figure}[!htb]
\centerline{\includegraphics[width=2.5in]{images/poti_pajeng_components.pdf}}
\hfil
\caption{Componentes Poti implementados.}
\label{fig:Poti_components}
\end{figure}


As informações do arquivo binário são salvas usando a biblioteca
LibRastro, uma biblioteca de geração de arquivos de rastro. A
LibRastro tem como principais características uma baixa intrusão na
coleta do rastro e flexibilidade nas definições de eventos,
características estas que coincidem com o nosso objetivo e requisitos
do formato Pajé. Temos então, um arquivo de rastro gerado na LibRastro
que contém informações equivalentes ao rastro original no formato
Pajé.

A biblioteca Poti -- responsável por implementar o formato de rastro
Pajé -- foi utilizada para a implementação da criação do rastro no
formato binário, usando como suporte a libRastro. Foram feitas
modificações na biblioteca com o objetivo de gerar a mesma saída Pajé
da Poti no novo formato binário proposto. É possível gerar o arquivo
com dois métodos distintos. Um deles é a tradução direta do arquivo
textual Pajé fazendo uso da libRastro, nomeado \texttt{standard}. O segundo método, chamado \texttt{reference}, busca obter uma
otimização quanto ao tamanho do arquivo gerado através do uso de
referências do tipo int short para dados do tipo string que costumam
se repetir durante o arquivo de rastro, gerando um arquivo binário de
tamanho reduzido.


Como a execução de aplicações em sistemas paralelos e distribuídos é
uma tarefa custosa, é de grande interesse promover ao usuário a
possibilidade de converter rastros gerados anteriormente.  Para isto,
foram criadas duas ferramentas dentro da Poti: \texttt{rst2paje} e
\texttt{paje2rst}.

A ferramenta \texttt{paje2rst} faz a tradução de arquivos do formato
textual Pajé gerados com a Poti para o novo formato binário. Na
implementação deste, foram usados Flex~\cite{Flex} e
Bison~\cite{Bison}, para fazer a análise léxica e sintática da entrada
textual, e as funções da nova biblioteca Poti para gerar o arquivo de
saída em formato binário equivalente.

A ferramenta \texttt{rst2paje} faz a tradução no sentido oposto, dado
o arquivo binário gerado na Poti, é gerado o arquivo no formato
textual equivalente. Para este foram usadas as funções de leitura de
arquivos da própria libRastro juntamente com funções que fazem a
organização dos dados de eventos de acordo com o formato textual.


A leitura e análise do novo formato é feita dentro da PajeNG, uma
ferramenta de visualização de rastros no formato Pajé. O componente
PajeRastroReader foi criado para realizar a decodificação dos arquivos
no formato binário proposto, tanto na versão \texttt{standard} quanto na versão
\texttt{reference}.

Para se tirar proveito da estrutura binária do formato, também foi
criado o componente PajeRastroTraceEvent que armazena os dados de
acordo com os tipos de dados usados na libRastro(int, doubles e
char). O objetivo deste componente é eliminar conversões de dados para
o formato string, tarefa essa que se torna muito custosa tendo em
vista o grande número de eventos que ocorre em um arquivo de
rastro. Outra tarefa da classe PajeRastroTraceEvent é a reordenação
dos dados dos eventos, que se torna necessária uma vez que a libRastro
faz o armazenamento dos dados em vetores de acordo com o tipo de dado,
perdendo a relação de ordem que existe entre o evento Pajé e sua
definição de cabeçalho, o que nos leva a ter uma decodificação diferenciada.


\subsection{Representação em binário do arquivo Pajé}\label{sec:codificacao}

 Um arquivo de rastro Pajé se divide em duas partes: definições de cabeçalho e eventos.
 As definições de cabeçalho contêm uma lista de tipos de eventos a serem registrados
 e seus parâmetros, definindo números de identificação, nome e os dados que 
 são usados como parâmetros. Logo após o cabeçalho, temos os eventos datados que foram
 registrados durante o rastro do programa, com o número de identificação do tipo de evento e os
 parâmetros, de acordo as regras definidas no cabeçalho.


 A libRastro, por ser uma biblioteca de criação de rastros, faz todos os seus registros
 como eventos, onde cada evento possui um valor numérico para identificação do seu 
 tipo, seguido de vetores com os dados que foram registrados.


 Abaixo temos um exemplo de uma definição de cabeçalho de um arquivo
 de rastro Pajé em seu formato textual.

\begin{verbatim} 
%EventDef PajeDefineContainerType 0
%    Alias string
%    Type string		
%    Name string
\end{verbatim}
Abaixo a versão binária do mesmo trecho de cabeçalho.

\begin{verbatim} 
type: 999
 strings-> {0}{11}{0}{3}{0}{2}{0}
\end{verbatim}
 No campo \texttt{type} é usado o valor 999 para identificar, durante a leitura, que o evento binário
 deve ser tratado como uma definição de cabeçalho.
 As informações de nome de campo e tipo de dado do cabeçalho Pajé são representadas por valores
 inteiros na versão binária, valores estes definidos em uma enumeração que faz parte da
 \PajeNG e da Poti.      
 No exemplo, o primeiro valor do vetor de dados é a identificação do evento, que é 
 o mesmo valor usado na representação textual (0, no exemplo), seguido por 
 tuplas nome do campo e tipo de dado. No exemplo, a primeira tupla da sequência é
 Alias e string, representadas no arquivo binário pelos valores 11, o valor da enumeração 
 para o campo Alias, e 0, valor da enumeração para o tipo de dado \texttt{string}. Os quatro valores 
 seguintes do vetor de dados em binário correspondem às outras duas tuplas, com os valores 3 e 2 
 representando os campos Type e Name respectivamente e 0 o tipo 
 string.
 
 
Abaixo, um exemplo de um evento datado no formato Pajé textual, evento este que corresponde à definição de cabeçalho anterior.
\begin{verbatim} 
0 1 0 HOST
\end{verbatim}
Abaixo temos a representação com a
libRastro para um evento datado obtido com o conversor \texttt{paje2rst} com o método \texttt{standard}.
\begin{verbatim} 
type: 0
 strings-> {1} {0} {HOST}
\end{verbatim}

Diferentemente da definição de cabeçalho, em eventos a informação do
campo \texttt{type} se refere à identificação do evento, com o valor 0 no exemplo indicando que o evento é do tipo
 \texttt{PajeDefineContainerType}. Em seguida, temos os vetores com os dados do evento
 datado agrupados de acordo com o seu tipo, neste caso todos em um vetor de strings.

A seguir temos a representação do mesmo evento datado obtido
também através do \texttt{paje2rst}, mas usando o método \texttt{reference}.
\begin{verbatim} 
type: 888
 strings-> {HOST}
type: 888
 strings-> {0} 
type: 888
 strings-> {1}
type: 0
 u_int16_ts-> {2} {1} {0}
\end{verbatim}
Os eventos em que o campo \texttt{type} tem o valor 888 definem que o evento 
contém uma string à ser usada como referência. No evento de exemplo temos três dados do tipo string, logo temos três eventos de referência que armazenam as strings.
 Abaixo destes eventos
existe o evento \Paje de fato, que faz referência às strings que são indicadas de acordo com a
ordem em que elas aparecem no arquivo como um valor inteiro de 16 bits.

 Embora um número maior de eventos seja gerado, temos uma redução significativa do tamanho total do arquivo com este método devido
a grande ocorrência de dados do tipo string.

\section{Resultados sobre o desempenho do
\texttt{PajeRastroReader}}\label{Resultados}


A análise de desempenho da leitura do formato binário foi feita sobre
dois arquivos de rastro de duas aplicações distintas, cada um em três
versões, textual -- a versão original -- , binária e binária reduzida
-- ambas obtidas com a ferramenta paje2rst. Uma das aplicações é uma
fatoração LU que gerou um arquivo de rastro de 0.77 GB, a outra é um
cálculo de gradiente conjugado com um rastro de tamanho 2.1 GB. A
máquina usada para os testes possui processador Intel i7-4770 CPU
(3.40GHz), HD 1TB SATA II (3.0Gb/s) e 8GiB DIMM DDR3(1.6GHz). Foram
feitas 30 execuções para cada versão, binárias e textual, dos dois
arquivos, de forma a se obter uma amostra significativa para
comparação sobre o desempenho do formato proposto.

%imagem aparece só na outra página, mas ainda não descobri uma solução
%melhor para isso
\begin{figure}[!htb]
\centerline{\includegraphics[width=2.5in]{images/cg_boxplot.pdf}} \hfil
\caption{Resultados dos experimentos com o arquivo de rastro gradiente conjugado %\review{Tu precisas quebrar esta
   % figura em duas. Uma com os tempos de leitura do rastro gradiente
  %  conjugado e outra figura com os tempos de leitura do rastro
 %   LU. Uma vez feito isso, tu terás dois comandos includegraphics. Um
 % para a primeira, outro para a segunda. Tenha certeza que tu quebras
  %a linha após o primeiro includegraphics com as duas barras, assim
 % eles ficarão em uma única coluna. Lembre-se que o melhor parâmetro
 % para o environmento figure é [!htb].}
 }
\label{fig:plot_cg}
\end{figure}

\begin{figure}[!htb]
\centerline{\includegraphics[width=2.5in]{images/lu_boxplot.pdf}} \hfil
\caption{Resultados dos experimentos com o arquivo de rastro LU
 }
\label{fig:plot_lu}
\end{figure}

Em ambos os gráficos, podemos notar um ganho de desempenho de aproximadamente 32\% do
formato binário \texttt{standard} em relação ao formato textual original. Com a versão \texttt{reference} o ganho foi de
27\% em relação ao formato textual. Sobre o tamanho do arquivo, a diferença de tamanho entre o arquivo original e o do método \texttt{reference} foi de 48\% para o arquivo de rastro da fatoração LU
e 43\% para o rastro do gradiente conjugado.


\section{Conclusões} Neste artigo apresentamos o formato binário
Pajé. Como esperado, a estrutura do arquivo binário promoveu uma
melhora no desempenho da leitura do arquivo de rastro. Esperava-se ganhos maiores com a
redução do tamanho do arquivo usando o método \texttt{reference}, mas o tempo
de processamento das referências acaba sendo maior que a simples leitura dos
dados em binário com o método \texttt{standard}. Ainda sim, ambos os métodos de geração de arquivos resultam em
um ganho de desempenho de leitura sobre o formato textual, reduzindo em até 32\% o tempo
de leitura para arquivos gerados com o método \texttt{standard}.

A partir destes resultados positivos, podemos buscar ainda outras
otimizações para o formato binário. Como possibilidade, temos o uso de
técnicas de compactação que tornariam o arquivo de rastro menor e
consequentemente um melhor tempo de leitura com técnicas de
descompactação eficientes aplicadas durante a leitura do arquivo
binário.


%-------------------------------------------------------------------------
\bibliographystyle{ieee} \bibliography{ieee}

\end{document}

